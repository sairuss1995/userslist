var elem =
  "Https://randomuser.me/api/?page=3&results=10&seed=abc&inc=gender,name,nat,picture,location";
fetch(elem)
  .then(function(res) {
    return res.json();
  })
  .then(function(data) {
    console.log(data);
    ren(data);
  });
function ren(data) {
  var root = document.querySelector("#root");
  console.log(root);
  data.results.map(function(open) {
    var div = document.createElement("div");
    var h2 = document.createElement("h2");
    var p = document.createElement("p");
    var img = document.createElement("img");
    var hidden = document.createElement("div");
    hidden.className = "hidden";
    h2.addEventListener("click", function() {
      var active = document.querySelector(".active");

      if (active && active !== this.nextSibling) {
        active.classList.toggle("active");
      }

      this.nextSibling.classList.toggle("active");
    });

    img.src = open.picture.medium;
    p.innerHTML =
      open.location.city +
      " " +
      open.location.country +
      " " +
      open.location.postcode;
    h2.innerHTML =
      open.name.title + " " + open.name.frist + " " + open.name.last;
    hidden.append(p);
    hidden.append(img);
    div.append(h2);
    div.append(hidden);
    root.append(div);
  });
}
